# README #

This is a sample project for creating UADI indicator tools using Java.

### What is this repository for? ###

* Quick summary

This project contains sample codes for developing UADI indicators in Java. 

"IndicatorGreenArea.java" is the main source code (for green area indicator) you need to investigate and implement.

"Utils.r" contains all utility functions to make the development as simple as possible. 

For more details about indicator tool implementation, check out the latest version of "UADI Indicator Dev Specs" document here:

https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-wps/downloads/


### How do I get set up? ###

* Summary of set up

Eclipse and Java 7 is requried to build and complie the indicator tool.

* How to run tests

It is a good practice for testing the code piece by piece before assambling them into the formal "execIndicator_YOUR_INDICATOR_NAME" function. 
If you want to have a preview of the outputs visualisation, just copy the json structure data such as:

{	"status":0,
	"errdesc":"",
	"data":{"geolayers":[...], "tables":[...], "charts":[...]}
}

and paste it here:

http://apps.csdila.ie.unimelb.edu.au/uadi/indicator/preview.html

For more details about indicator tool outputs, check this link:

https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-wps/wiki/Home 

### Who do I talk to? ###

Dr Yiqun Chen

Centre for Disaster Management & Public Safety

Centre for Spatial Data Infrastructures & Land Administration

The University of Melbourne

E: yiqun.c@unimelb.edu.au