/*
 *  Copyright 2016-2017 University of Melbourne
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
 */

import org.junit.Test;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.MultiPolygon;

import uadi.indicator.wps.uom.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class IndicatorTest {

	public IndicatorTest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Test indicator implementation logic here
	 */
	//@Test
	public void execIndicatorTest() {

		System.setProperty("org.geotools.referencing.forceXY", "true");
	
		try {
			if (!Utils.isGeoServerReachable()) {
				System.out.println("== GeoServer is not reachable.");
				return;
			}

			String url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=12&ca={\"caASGSCode\":\"24600\",\"caASGSType\":\"lga\",\"caBuffer\":-0.00001}";
			
			SimpleFeatureCollection fc = Utils.getFeatureCollection(url);
			
			String outGeomName = "the_geom";
			
			// build output feature type
			SimpleFeatureTypeBuilder stbOutput = new SimpleFeatureTypeBuilder();
			stbOutput.setName("Output");
			stbOutput.setCRS(fc.getSchema().getCoordinateReferenceSystem());
			stbOutput.add(outGeomName, MultiPolygon.class);
			stbOutput.setDefaultGeometry(outGeomName);
			
			// copy attributes from msu population datalayer
			for (AttributeDescriptor attDisc : fc.getSchema().getAttributeDescriptors()) {
				String name = attDisc.getLocalName();
				Class type = attDisc.getType().getBinding();
				if (attDisc instanceof GeometryDescriptor) {
					// skip existing geometry field since we have created a new
					// one 'the_geom'
				} else {
					// try to keep all rest fields, since the field name cannot
					// extend 10 chars, trim it if too long.
					stbOutput.add(Utils.clapFieldName(name), type);
				}
			}


			// prepare output FeatureCollection
			SimpleFeatureType sftOutput = stbOutput.buildFeatureType();
			SimpleFeatureBuilder sfbOutput = new SimpleFeatureBuilder(sftOutput);
			DefaultFeatureCollection fcOutput = new DefaultFeatureCollection();

			SimpleFeatureIterator itr = fc.features();


			while (itr.hasNext()) {

				SimpleFeatureImpl f = (SimpleFeatureImpl) itr.next();

			
				SimpleFeature newf = sfbOutput.buildFeature(null);

				for (AttributeDescriptor attDisc : fc.getSchema().getAttributeDescriptors()) {
					String name = attDisc.getLocalName();
					// if current field is a geometry, we save it to the
					// appointed field 'the_geom'
					if (attDisc instanceof GeometryDescriptor) {
						newf.setAttribute("the_geom", f.getDefaultGeometry());
					} else // otherwise, copy field value respectively
					{
						newf.setAttribute(Utils.clapFieldName(name), f.getAttribute(name));
					}
				}

				// add newly built feature to FeatureCollection
				fcOutput.add(newf);
			}
			itr.close();


			String wfsurl = Utils.publishFeatureCollection(fcOutput, "testout");
			
			//create output 
			JSONObject data = new JSONObject();
			JSONArray geolayerArr = new JSONArray();
			
			JSONObject geolayer = new JSONObject();
			geolayer.put("layerdisplayname", "greenarea_msu");
			geolayer.put("layername", Utils.getLayerNameFromWFSUrl(wfsurl));
			geolayer.put("bbox", Utils.getBbox(fc));
			
			//wfs part
			JSONObject wfs_part = new JSONObject();
			wfs_part.put("url", wfsurl);
			/*
			//if necessary, get wfs style parameters created
			JSONObject caWFSStyleResult = Utils.createWFSStyle(output_ca_wfsurl, "idxval");
			if(caWFSStyleResult!=null && caWFSStyleResult.getInt("status") == 0){
				wfs_part.put("styleparams", caWFSStyleResult.getJSONObject("data"));
			}
			*/
			//or, just skip it
			wfs_part.put("styleparams", new JSONObject());
			
			geolayer.put("wfs", wfs_part);
			
			//wms part, get wms style parameters created
			JSONObject WMSStyleResult = Utils.createWMSStyle(wfsurl, "total_popu", "Greens", false, "polygon", 9, "Jenks");
			if(WMSStyleResult!=null && WMSStyleResult.getInt("status") == 0){
				JSONObject wms = new JSONObject();
				wms.put("url", Utils.getWMSUrlTemplate());
				wms.put("styleparams", WMSStyleResult.getJSONObject("data"));
				geolayer.put("wms", wms);
			}

			geolayerArr.put(geolayer);
			
			data.put("geolayers", geolayerArr);
			JSONObject output = new JSONObject();
			output.put("data", data);
			output.put("status", 0);
			
			
			System.out.println("=== output:"+output.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("== execIndicator test done");
		return;
	}

	/**
	 * Test UoW Indicator
	 */
	@Test
	public void execUoWWaterIndicatorTest() {

		System.setProperty("org.geotools.referencing.forceXY", "true");
		
		JSONObject output = new JSONObject();
		try {
			if (!Utils.isGeoServerReachable()) {
				System.out.println("== GeoServer is not reachable.");
				return;
			}

			String source_wfsurl = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9&ontomapid=93&ca={%22caBuffer%22:-1.0E-5,%22caASGSCode%22:%22107%22,%22caASGSType%22:%22sa4%22}";
			String sa2_wfsurl = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9&ontomapid=94&ca={%22caBuffer%22:-1.0E-5,%22caASGSCode%22:%22107%22,%22caASGSType%22:%22sa4%22}";
			
			SimpleFeatureCollection fcISE = Utils.getFeatureCollection(source_wfsurl);
			
			SimpleFeatureCollection fcSA2 = Utils.getFeatureCollection(sa2_wfsurl);

			String usage_type = "residential";
			
			int ref_year = 2011;
			//########################################
			// Core indicator calculation logic starts 
			//########################################
			// change this section to your own indicator implmentation
			if (ref_year <= 2006 || ref_year > 2013)
				ref_year = 2011;

			// the output geometry attribute name has to be set as "the_geom", 
			// otherwise GeoTools lib will not create a shpfile from the constructed output SimpleFeatureCollection
			String outGeomName = "the_geom";
			String outName_pcpw = "pcpw_l"; // Per Capita Residential Potable Water (l)
			String outName_ttlpw = "ttlpw_l"; // Total Residential Potable Water (l)
			String outName_ttlpop = "ttlpop"; // Total population

			// build output feature type, which contains the geom
			SimpleFeatureTypeBuilder stbISEOutput = new SimpleFeatureTypeBuilder();
			stbISEOutput.setName("Output");
			stbISEOutput.setCRS(fcSA2.getSchema().getCoordinateReferenceSystem());
			stbISEOutput.add(outGeomName, MultiPolygon.class);
			stbISEOutput.setDefaultGeometry(outGeomName);
			
			// add new attributes for holding the indicator value
			stbISEOutput.add(outName_pcpw, Double.class);
			stbISEOutput.add(outName_ttlpw, Double.class);
			stbISEOutput.add(outName_ttlpop, Integer.class);
			stbISEOutput.add("sa2_code", String.class);
			stbISEOutput.add("sa2_name", String.class);

			// prepare output FeatureCollection
			SimpleFeatureType sftISEOutput = stbISEOutput.buildFeatureType();
			SimpleFeatureBuilder sfbISEOutput = new SimpleFeatureBuilder(sftISEOutput);
			DefaultFeatureCollection fcISEOutput = new DefaultFeatureCollection();

			SimpleFeatureIterator itrISE = fcISE.features();

			JSONObject sa2AggValue = new JSONObject();
			JSONObject sa2Population = new JSONObject();

			while (itrISE.hasNext()) {
				SimpleFeatureImpl f = (SimpleFeatureImpl) itrISE.next();
				try {
					JSONObject hasTime = new JSONObject(f.getAttribute("hasTime").toString());
					int yearNo = hasTime.getJSONObject("Time").getInt("year");
					
					JSONObject hasBoundary = new JSONObject(f.getAttribute("hasBoundary").toString());
					String sa2_code = hasBoundary.getJSONObject("SA2").get("sa2_5digit_code").toString();
					String sa2_name = hasBoundary.getJSONObject("SA2").get("sa2_name").toString();
					
					String usage_type_string = f.getAttribute("usage_type").toString();
					
					if (ref_year == yearNo && usage_type_string.equalsIgnoreCase(usage_type)) {
						
						double usage_value = Double.parseDouble(f.getAttribute("usage_value").toString());
						
						if(sa2AggValue.has(sa2_code)){
							double val = usage_value + sa2AggValue.getDouble(sa2_code);
							sa2AggValue.put(sa2_code, val);
						}else
						{
							sa2AggValue.put(sa2_code, usage_value);
						}
						
						if(!sa2Population.has(sa2_code)){
							sa2Population.put(sa2_code, Integer.parseInt((f.getAttribute("population").toString())));
						}
	
					}
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
			itrISE.close();
						
			//all values are ready, assemble it to featurecollection
			Double MISSINGVALUE = 0.0; // using 0.0 for missing or null value
			SimpleFeatureIterator itrSA2 = fcSA2.features();

			while (itrSA2.hasNext()) {
				SimpleFeatureImpl f = (SimpleFeatureImpl) itrSA2.next();
				SimpleFeature newf = sfbISEOutput.buildFeature(null);
				
				newf.setAttribute("the_geom", f.getDefaultGeometry());
				
				String sa2_code = f.getAttribute("sa2_5digit_code").toString();
				String sa2_name = f.getAttribute("sa2_name").toString();

				newf.setAttribute("sa2_code" , sa2_code);
				newf.setAttribute("sa2_name" , sa2_name);
				
				if(sa2Population.has(sa2_code)){
					int pop = sa2Population.getInt(sa2_code);
					newf.setAttribute(outName_ttlpop , pop);
					newf.setAttribute(outName_ttlpw , sa2AggValue.getDouble(sa2_code));
					
					if(pop >0){
						newf.setAttribute(outName_pcpw ,  sa2AggValue.getDouble(sa2_code)/pop);
					}else
					{
						newf.setAttribute(outName_pcpw , MISSINGVALUE);
					}
				}
				else
				{
					//skip a sa2 if no water data does not exist
					continue;
				}
				

				// add newly built feature to FeatureCollection
				fcISEOutput.add(newf);
			}
			itrSA2.close();
			
			//publish to server
			String output_wfsurl = Utils.publishFeatureCollection(fcISEOutput, "pcpw");
			
			System.out.println("===output_wfsurl:"+output_wfsurl);
			//wrap up outputs
			JSONObject data = new JSONObject();
			
			//handle geolayers
			JSONArray geolayerArr = new JSONArray();
			
			JSONObject geolayer = new JSONObject();
			geolayer.put("layerdisplayname", "pcpw_"+ref_year+"_"+usage_type);
			geolayer.put("layername", Utils.getLayerNameFromWFSUrl(output_wfsurl));
			geolayer.put("bbox", Utils.getBbox(fcISEOutput));
			
			//wfs part
			JSONObject wfs = new JSONObject();
			wfs.put("url", output_wfsurl);
			
			//if necessary, get wfs style parameters created
			JSONObject WFSStyleResult = Utils.createWFSStyle(output_wfsurl, outName_pcpw);
			if(WFSStyleResult!=null && WFSStyleResult.getInt("status") == 0){
				wfs.put("styleparams", WFSStyleResult.getJSONObject("data"));
			}
			
			
			geolayer.put("wfs", wfs);
			
			//wms part, get wms style parameters created
			//ATTENTION: wms style MUST be created so that the layer can be visualised in UADI.
			JSONObject WMSStyleResult = Utils.createWMSStyle(output_wfsurl, outName_pcpw, "Blues", false, "polygon", 9, "Jenks");
			if(WMSStyleResult!=null && WMSStyleResult.getInt("status") == 0){
				JSONObject wms = new JSONObject();
				wms.put("url", Utils.getWMSUrlTemplate());
				wms.put("styleparams", WMSStyleResult.getJSONObject("data"));
				geolayer.put("wms", wms);
			}
			
			geolayerArr.put(geolayer);
			
			data.put("geolayers", geolayerArr);
			
			
			// charts
			JSONArray charts = new JSONArray();
			
			JSONObject chart = new JSONObject();
			chart.put("type", "linechart");
			chart.put("seqnum",  1);
			chart.put("title", "Residential potable consumption per capita (L)");
			chart.put("stacked", false);
			chart.put("xfield", "sa2_code");
			chart.put("yfield", (new JSONArray()).put("consumption"));
			chart.put("yfieldtitle", (new JSONArray()).put("consumption"));
			
			JSONArray iseData = new JSONArray();
			
			Iterator<?> keys = sa2AggValue.keys();
			while(keys.hasNext() ) {
			    String key = (String)keys.next();
			    
			    int pop = sa2Population.getInt(key);

			    double consumption = 0.0;
				if(pop >0){
					consumption = sa2AggValue.getDouble(key)/pop;
				}else
				{
					consumption =  MISSINGVALUE;
				}
				
			    JSONObject dataCell = new JSONObject();
				dataCell.put("sa2_code", key);
				dataCell.put("consumption", consumption);
				iseData.put(dataCell);
			}
			
			//sort jsonarray by consumption value
			JSONArray iseDataSorted = new JSONArray();

		    List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		    for (int i = 0; i < iseData.length(); i++) {
		        jsonValues.add(iseData.getJSONObject(i));
		    }
		    Collections.sort( jsonValues, new Comparator<JSONObject>() {
		        //You can change "consumption" with "sa2_code" if you want to sort by sa2_code
		        private static final String KEY_NAME = "consumption";

		        @Override
		        public int compare(JSONObject a, JSONObject b) {
		            Double valA = null;
		            Double valB = null;

		            try {
		                valA = (Double) a.get(KEY_NAME);
		                valB = (Double) b.get(KEY_NAME);
		            } 
		            catch (Exception e) {
		                //do something
		            }

		            return valA.compareTo(valB);
		            //if you want to change the sort order, simply use the following:
		            //return -valA.compareTo(valB);
		        }
		    });

		    for (int i = 0; i < jsonValues.size(); i++) {
		    	iseDataSorted.put(jsonValues.get(i));
		    }
			
			chart.put("data", iseDataSorted);
			charts.put(chart);
			data.put("charts", charts);

			output.put("data", data);
			output.put("status", 0);

		} catch (Exception e) {
			output.put("errdesc", e.getMessage());
			output.put("status", 1);
		}

		//########################################
		// Sync indicator outputs to an indicator job
		//########################################
		// update job outputs if jobuuid is provided
		//if (jobuuid != null) {
		//	Utils.updateJob(output, jobuuid);
		//}

		System.out.println(output.toString());
			
		return;
	}
}
