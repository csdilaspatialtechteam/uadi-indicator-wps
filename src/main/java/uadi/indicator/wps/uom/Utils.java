/*
 *  Copyright 2016-2017 University of Melbourne
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
 */

package uadi.indicator.wps.uom;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.FeatureCollection;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.referencing.CRS;
import org.json.JSONArray;
import org.json.JSONObject;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import it.geosolutions.geoserver.rest.GeoServerRESTPublisher;
import it.geosolutions.geoserver.rest.GeoServerRESTReader;
import it.geosolutions.geoserver.rest.GeoServerRESTPublisher.UploadMethod;
import it.geosolutions.geoserver.rest.encoder.GSResourceEncoder.ProjectionPolicy;
import sun.misc.BASE64Decoder;

public class Utils {

	public Utils() {
		// TODO Auto-generated constructor stub
	}

	// ATTENTION:
	// devKey is used for storing and publishing indicator outputs in UADI GeoServers so that the outputs can be viewed, used, downloaded by others
	// To obtain a devKey for UADI indicator development, please contact UoM UADI dev team: 
	// Benny Chen: yiqun.c@unimelb.edu.au 
	// Sam Amirebrahimi: amis@unimelb.edu.au
	public final static String devKey = "";
	public final static String BaseServiceUrl = "http://apps.csdila.ie.unimelb.edu.au/uadi-service";
	public final static String credUrl = BaseServiceUrl + "/execengine/getgscredentials?devkey=";
	public final static String jobUpdateUrl = BaseServiceUrl + "/execengine/indicator/job/update";
	public final static String WMSStyleCreateUrl =BaseServiceUrl + "/styling/wms/create";
	public final static String WFSStyleCreateUrl =BaseServiceUrl + "/styling/wfs/create";


	private static String tempDirPath = "";
	private static String wfsUrlTemplate = "";
	private static String wmsUrlTemplate = "";
	private static String gsRESTURL = "";
	private static String gsRESTUSER = "";
	private static String gsRESTPW = "";
	

	// ATTENTION: 
	// gsWORKSPACENAME and gsDATASTORESNAME are pre-created to match with the above gsRESTUSER, changing their
	// values will fail the geoserver data publication service.
	private static String gsWORKSPACENAME = "";
	private static String gsDATASTORESNAME = "";

	private static GeoServerRESTPublisher publisher;
	private static GeoServerRESTReader reader;


	/**
	 * get GeoServer Credentials from server and initialise variables
	 */
	public static boolean initGeoServerCredentials() {

		InputStream in = null;
		StringWriter writer = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(credUrl + devKey);

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			in = (InputStream) connection.getInputStream();
			writer = new StringWriter();
			IOUtils.copy(in, writer);

			JSONObject output = new JSONObject(writer.toString());

			// the GeoServer Credentials is encrypted and stored in "data"
			// attribute, decrypt it before parsing
			String encryptedOutput = decrypt(output.getString("data"), devKey.substring(0, 16));

			// System.out.println("=== encryptedOutput: "+encryptedOutput);

			JSONObject gsCredentials = new JSONObject(encryptedOutput);

			tempDirPath = gsCredentials.getString("tempDirPath");
			wfsUrlTemplate = gsCredentials.getString("wfsUrlTemplate");
			gsRESTURL = gsCredentials.getString("gsRESTURL");
			gsRESTUSER = gsCredentials.getString("gsRESTUSER");
			gsRESTPW = gsCredentials.getString("gsRESTPW");
			gsWORKSPACENAME = gsCredentials.getString("gsWORKSPACENAME");
			gsDATASTORESNAME = gsCredentials.getString("gsDATASTORESNAME");
			wmsUrlTemplate = gsRESTURL + "/" + gsWORKSPACENAME + "/wms";

			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				in.close();
			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (connection != null)
				connection.disconnect();
		}

		return false;
	}

	/**
	 * Test if GeoServer can be reached, this method does the GeoServer reader
	 * and publisher initialisation.
	 * 
	 * @return true or false
	 */
	public static boolean isGeoServerReachable() {

		// if reader has already initialised, return true.
		if (reader != null)
			return true;

		try {

			if (!initGeoServerCredentials()) {
				System.out.println("=== geoserver credentials initialisation error");
				return false;
			}

			reader = new GeoServerRESTReader(gsRESTURL, gsRESTUSER, gsRESTPW);
			publisher = new GeoServerRESTPublisher(gsRESTURL, gsRESTUSER, gsRESTPW);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("=== geoserver connection error");
			return false;
		}

		if (reader == null || !reader.existGeoserver()) {
			System.out.println("=== geoserver is not running");
			return false;
		}

		return true;
	}

	/**
	 * publish a simplefeaturecollection to geoserver, if success, return the
	 * published layers wfs getfeature url
	 * 
	 * @param fc
	 * @param layerNamePrefix
	 * @return
	 * @throws IOException
	 * @throws FactoryException
	 */
	public static String publishFeatureCollection(SimpleFeatureCollection fc, String layerNamePrefix)
			throws IOException, FactoryException {

		// get a random layername
		String uuid = UUID.randomUUID().toString();

		File shpfile = new File(String.format("%s%s%s", tempDirPath, File.separator, layerNamePrefix + "_" + uuid));

		if (shpfile.exists()) {
			FileUtils.deleteDirectory(shpfile);
		}
		shpfile.mkdirs();

		String outputShpFilePath = shpfile.getAbsolutePath() + File.separator + layerNamePrefix + "_" + uuid + ".shp";

		Utils.featuresExportToShapeFile(fc, new File(outputShpFilePath), true);

		// zip it
		String zipFilePath = shpfile.getAbsolutePath() + ".zip";
		List<String> zipFileList = new ArrayList<String>();		
		zipIt(zipFilePath, shpfile.getAbsolutePath(), zipFileList);
		File zipFile = new File(zipFilePath);

		String crsString = fc.getSchema().getCoordinateReferenceSystem().getIdentifiers().toString();
		crsString = crsString.replaceAll("\\[", "").replaceAll("\\]", "");

		// publish it to geoserver
		String url = Utils.publishZippedShp(layerNamePrefix + "_" + uuid, crsString, crsString, zipFile);

		// clean up
		FileUtils.deleteDirectory(shpfile);
		FileUtils.forceDelete(zipFile);

		return url;
	}

	/**
	 * publish a zipped shp file to geoserver
	 * 
	 * @param layer
	 * @param declaredSRS
	 * @param nativeSRS
	 * @param zipFile
	 * @return
	 */
	public static String publishZippedShp(String layer, String declaredSRS, String nativeSRS, File zipFile) {

		if (!isGeoServerReachable()) {
			return "";
		}

		boolean published = true;
		try {
			// check if target workspace exists, if not,create a new workspace
			// ATTENTION: this actually should never be checked. workspace must be created. 
			// the geoserver user account obtained from initGeoServerCredentials() doesn't have privilege to create a new workspace
			/*List<String> existingWorkSpaces = reader.getWorkspaceNames();
			if (!existingWorkSpaces.contains(gsWORKSPACENAME)) {
				System.out.println("===this should never be reached, since workspace:" + gsWORKSPACENAME
						+ " should be created");
				publisher.createWorkspace(gsWORKSPACENAME);
			}
			*/
			
			// check if target layer exists, if yes, unpublish it and then remove the existing one first
			// ATTENTION: if layer does not exist, this line (reader.getLayer())  will always raise a "No such layer" error and print trace log for "org.geoserver.rest.RestletException" 
			// in the GeoServer log. if layer name is unique (e.g., based on uuid), there is no need to perform this check, and skip this part to safely. 
			/*
			if (reader.getLayer(gsWORKSPACENAME, layer) != null) {
				publisher.unpublishFeatureType(gsWORKSPACENAME, gsDATASTORESNAME, layer);
				publisher.removeLayer(gsWORKSPACENAME, layer);
			}
			*/

			// publish shp file with srs projection
			published = publisher.publishShp(gsWORKSPACENAME, gsDATASTORESNAME, null, layer,
					UploadMethod.FILE, zipFile.toURI(), declaredSRS, nativeSRS, ProjectionPolicy.FORCE_DECLARED,
					null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("=== " + e.getMessage());
		}

		if (!published) {
			return "";
		}

		return String.format(wfsUrlTemplate, gsRESTURL, gsWORKSPACENAME, layer);
	}

	/**
	 * get SimpleFeatureCollection object by parsing wfs feed
	 * 
	 * @param wfsurl
	 * @return
	 * @throws IOException
	 */
	public static SimpleFeatureCollection getFeatureCollection(String wfsurl) throws IOException {

		FeatureCollection features = null;
		FeatureJSON fjson = new FeatureJSON();
		InputStream is = null;

		//devkey must be attached to access data, otherwise empty geojson will be returned.
		wfsurl = wfsurl + "&devkey=" + devKey;
		
		try {

			URL url = new URL(wfsurl);
			is = url.openStream();
			features = fjson.readFeatureCollection(is);
			if (features.getSchema().getCoordinateReferenceSystem() != null) {
				fjson.setEncodeFeatureCollectionBounds(true);
				fjson.setEncodeFeatureCollectionCRS(true);
			}

		} catch (Exception e) {

		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return (SimpleFeatureCollection) features;
	}
	
	public static void updateJob(JSONObject joboutputs, String jobuuid) {

		InputStream in = null;
		StringWriter writer = null;
		HttpURLConnection connection = null;
		try {
			
			Map<String,Object> params = new LinkedHashMap<>();
	        params.put("jobuuid", jobuuid);
	        params.put("joboutputs", joboutputs.toString());
	        params.put("devkey", devKey);

	        StringBuilder postData = new StringBuilder();
	        for (Map.Entry<String,Object> param : params.entrySet()) {
	            if (postData.length() != 0) postData.append('&');
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }
	        byte[] postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

	        URL url = new URL(jobUpdateUrl);
	        connection = (HttpURLConnection)url.openConnection();
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
	        connection.setDoOutput(true);
	        connection.getOutputStream().write(postDataBytes);
	        
	        in = (InputStream) connection.getInputStream();
			writer = new StringWriter();
			System.out.println("=== job update: "+writer.toString());

			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				in.close();
			}

		} catch (Exception e) {
			System.out.println("=== job update error: "+ e.getMessage());
		} finally {

			if (connection != null)
				connection.disconnect();
		}

		return;
	}

	/**
	 * clap shp file column name to 10 chars
	 * also replace "-" to "_" in the column name (it is common to see "-" in concept property name )
	 * @param name
	 * @return
	 */

	public static String clapFieldName(String name) {

		
		name = name.replaceAll("-", "_");
		
		if (name.length() > 10)
			return name.substring(0, 10);
		else
			return name;
	}

	/**
	 * export SimpleFeatureCollection to shp file and store it locally
	 * 
	 * @param fc
	 * @param newFile
	 * @param createSchema
	 * @throws IOException
	 * @throws FactoryException
	 */

	public static void featuresExportToShapeFile(SimpleFeatureCollection fc, File newFile, boolean createSchema)
			throws IOException, FactoryException {

		if (!newFile.exists()) {
			newFile.createNewFile();
		}
		ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

		Map<String, Serializable> params = new HashMap<String, Serializable>();
		params.put("url", newFile.toURI().toURL());
		params.put("create spatial index", Boolean.TRUE);

		ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
		if (createSchema) {
			newDataStore.createSchema(fc.getSchema());
		}

		newDataStore.forceSchemaCRS(fc.getSchema().getCoordinateReferenceSystem());
		Transaction transaction = new DefaultTransaction("create");
		String typeName = newDataStore.getTypeNames()[0];
		SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);

		if (featureSource instanceof SimpleFeatureStore) {
			SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
			featureStore.setTransaction(transaction);
			try {
				featureStore.addFeatures(fc);
				transaction.commit();

			} catch (Exception problem) {
				System.out.println("=== exception occurs, rolling back transaction in feature store");
				transaction.rollback();
			} finally {
				transaction.close();
			}
		} else {
			System.out.println("=== " + typeName + " does not support read/write access");
		}

	}

	/**
	 * decrypt encrypted data with a key
	 * 
	 * @param encryptedData
	 * @param keyValue
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String encryptedData, String keyValue) throws Exception {
		Key key = generateKey(keyValue);
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	/**
	 * get key from provided keyvalue
	 * 
	 * @param keyValue
	 * @return
	 * @throws Exception
	 */
	private static Key generateKey(String keyValue) throws Exception {

		return new SecretKeySpec(keyValue.getBytes(), "AES");
	}

	/**
	 * autmatically determine a UTM CRS from a given SimpleFeatureCollection
	 * 
	 * @param fc
	 * @return
	 * @throws NoSuchAuthorityCodeException
	 * @throws FactoryException
	 */
	public static CoordinateReferenceSystem getAutoUTM(SimpleFeatureCollection fc)
			throws NoSuchAuthorityCodeException, FactoryException {

		double lon = (fc.getBounds().getMaxX() + fc.getBounds().getMinX()) / 2.0;
		double lat = (fc.getBounds().getMaxY() + fc.getBounds().getMinY()) / 2.0;

		CoordinateReferenceSystem crs = CRS.decode("AUTO2:42001," + lon + "," + lat);

		return crs;
	}
	
	
	/**
	 * zip files
	 * @param zipFile
	 * @param sourceFolder
	 * @param zipFileList
	 */
	public static void zipIt(String zipFile, String sourceFolder, List<String> zipFileList){
		
		zipGenerateFileList(new File(sourceFolder), sourceFolder, zipFileList);

		byte[] buffer = new byte[1024];

		try{

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			//System.out.println("=== output to Zip : " + zipFile);

			for(String file : zipFileList){

				//System.out.println("File Added : " + file);
				ZipEntry ze= new ZipEntry(file);
				zos.putNextEntry(ze);

				FileInputStream in = 
						new FileInputStream(sourceFolder + File.separator + file);

				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}

				in.close();
			}

			zos.closeEntry();
			zos.close();

			//System.out.println("Done");
		}catch(IOException ex){
			ex.printStackTrace();   
		}
	}
	
	/**
	 * Traverse a directory and get all files, and add the file into fileList  
	 * @param node
	 * @param sourceFolder
	 * @param zipFileList
	 */
	public static void zipGenerateFileList(File node, String sourceFolder, List<String> zipFileList){

		//add file only
		if(node.isFile()){
			String filePath = node.getAbsoluteFile().toString();
			filePath.substring(sourceFolder.length()+1, filePath.length());
			zipFileList.add(filePath.substring(sourceFolder.length()+1, filePath.length()));
		}

		//add directory
		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				zipGenerateFileList(new File(node, filename), sourceFolder, zipFileList);
			}
		}

	}
	
	/**
	 * get the bounding box of a SimpleFeatureCollection [minx, miny, maxx, maxy]
	 * @param fc
	 * @return
	 */
	public static JSONArray getBbox(SimpleFeatureCollection fc){
		
		JSONArray bbox = new JSONArray();
		if(fc.getBounds()!=null){
			bbox.put(fc.getBounds().getMinX());
			bbox.put(fc.getBounds().getMinY());
			bbox.put(fc.getBounds().getMaxX());
			bbox.put(fc.getBounds().getMaxY());
		}
		return bbox;
	}
	
	/**
	 * 
	 * @param wfsurl: wfs url for the data layer
	 * @param attrname: the attribute name that the style will be created for.
	 * @param palettename
	 * check colors defined under each palette name at http://colorbrewer2.org
	 * acceptable color PaletteNames are: YlOrRd: 9 colors PRGn: 11 colors PuOr:
	 * 11 colors RdGy: 11 colors Spectral: 11 colors Grays: 9 colors PuBuGn: 9
	 * colors RdPu: 9 colors BuPu: 9 colors YlOrBr: 9 colors Greens: 9 colors
	 * BuGn: 9 colors Accents: 8 colors GnBu: 9 colors PuRd: 9 colors Purples: 9
	 * colors RdYlGn: 11 colors Paired: 12 colors Blues: 9 colors RdBu: 11
	 * colors Oranges: 9 colors RdYlBu: 11 colors PuBu: 9 colors OrRd: 9 colors
	 * Set3: 12 colors Set2: 8 colors Set1: 9 colors Reds: 9 colors PiYG: 11
	 * colors Dark2: 8 colors YlGn: 9 colors BrBG: 11 colors YlGnBu: 9 colors
	 * Pastel2: 8 colors Pastel1: 9 colors
	 * 
	 * @param colorreverseorder： true or false(default), whether to reverse the color order defined in a palette
	 * @param geomtype
	 * acceptable geomtypes are: point, multipoint, polygon, multipolygon, linestring, multilinestring, geometry
	 * @param colornum: the number of colors to be classified. default 5.  in range of 2-15
	 * @param classifierName: classifier function name 
	 * acceptable classifier function names are: Jenks(default), EqualInterval, Quantile, StandardDeviation 
	 * @return
	 */
	public static JSONObject createWMSStyle(String wfsurl, String attrname, String palettename, boolean colorreverseorder, String geomtype, int colornum, String classifier) {

		JSONObject output = null;
		
		InputStream in = null;
		StringWriter writer = null;
		HttpURLConnection connection = null;
		try {
			
			wfsurl = wfsurl + "&propertyName=" + attrname;
			String createStyleUrl = WMSStyleCreateUrl + "?devkey="+devKey;
			createStyleUrl += "&attrname="+attrname;
			createStyleUrl += "&palettename="+palettename;
			createStyleUrl += "&colorreverseorder="+colorreverseorder;
			createStyleUrl += "&geomtype="+geomtype;
			createStyleUrl += "&colornum="+colornum;
			createStyleUrl += "&classifier="+classifier;
			createStyleUrl += "&url="+URLEncoder.encode(wfsurl, "UTF-8");
					
			URL url = new URL(createStyleUrl);

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			in = (InputStream) connection.getInputStream();
			writer = new StringWriter();
			IOUtils.copy(in, writer);

			output = new JSONObject(writer.toString());

			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				in.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (connection != null)
				connection.disconnect();
		}

		return output;
	}
	
	/**
	 * check if a classifer name is valid
	 * @param classifier
	 * @return
	 */
	public static boolean isValidClassifier(String classifier){
		boolean flag = false;
		String strValidClassifiers = "Jenks,EqualInterval,Quantile,StandardDeviation";
		String[] arrValidClassifiers = strValidClassifiers.split(",");
		for(int i=0;i<arrValidClassifiers.length;i++){
			if(classifier.equals(arrValidClassifiers[i])){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * check if a color name is valid
	 * @param colorname
	 * @return
	 */
	public static boolean isValidColorName(String colorname){
		boolean flag = false;
		String strValidColors = "Reds,Greens,Blues,YlOrRd,PRGn,PuOr,RdGy,Spectral,Grays,PuBuGn,RdPu,BuPu,YlOrBr,BuGn,Accents,GnBu,PuRd,Purples,RdYlGn,Paired,RdBu,Oranges,RdYlBu,PuBu,OrRd,Set3,Set2,Set1,PiYG,Dark2,YlGn,BrBG,YlGnBu,Pastel2,Pastel1";
		String[] arrValidColors = strValidColors.split(",");
		for(int i=0;i<arrValidColors.length;i++){
			if(colorname.equals(arrValidColors[i])){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * check if a color number is valid
	 * @param colornum
	 * @return
	 */
	public static boolean isValidColorNum(int colornum){
		
		if(colornum<1 || colornum>9) return false;
		return true;
	}
	
	/**
	 * create wfs style parameters, such as min, max, mean, sd. 
	 * these parameters can be used for automatic client-side style creation
	 * @param wfsurl
	 * @param attrname
	 * @return
	 */
	public static JSONObject createWFSStyle(String wfsurl, String attrname) {

		JSONObject output = null;
		
		InputStream in = null;
		StringWriter writer = null;
		HttpURLConnection connection = null;
		try {
			
			wfsurl = wfsurl + "&propertyName=" + attrname;
			String createStyleUrl = WFSStyleCreateUrl + "?devkey="+devKey;
			createStyleUrl += "&attrname="+attrname;
			createStyleUrl += "&url="+URLEncoder.encode(wfsurl, "UTF-8");
					
			URL url = new URL(createStyleUrl);

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			in = (InputStream) connection.getInputStream();
			writer = new StringWriter();
			IOUtils.copy(in, writer);

			output = new JSONObject(writer.toString());

			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				in.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			if (connection != null)
				connection.disconnect();
		}

		return output;
	}
	
	/**
	 * get wms url for current devkey workspace
	 * @return
	 */
	public static String getWMSUrlTemplate(){
		return wmsUrlTemplate;
	}
	
	/**
	 * parse wfs url and return its 'typeName' section
	 * @param wfsurl
	 * @return
	 */
	public static String getLayerNameFromWFSUrl(String wfsurl){
		String result = "";
		String[] components = wfsurl.split("&");
		for(int i=0;i<components.length;i++){
			if(components[i].toLowerCase().startsWith("typename=")){
				result = components[i].split("=")[1];
				break;
			}
		}
		return result;
	}
}
