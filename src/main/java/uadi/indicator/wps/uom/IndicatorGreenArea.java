/*
 *  Copyright 2016-2017 University of Melbourne
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
 */
package uadi.indicator.wps.uom;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import org.geotools.data.crs.ForceCoordinateSystemFeatureResults;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ReprojectingFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.process.factory.DescribeParameter;
import org.geotools.process.factory.DescribeProcess;
import org.geotools.process.factory.DescribeResult;
import org.geotools.process.factory.StaticMethodsProcessFactory;
import org.geotools.text.Text;
import org.json.*;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class IndicatorGreenArea extends StaticMethodsProcessFactory<IndicatorGreenArea> {

	public IndicatorGreenArea() {
		//set title and namespace for a process, the namespace here has to be set as "custom"
		super(Text.text("UADI Indicator GreenArea"), "custom", IndicatorGreenArea.class);
	}

	@DescribeProcess(title = "execIndicatorGreenArea", description = "calculate greenarea index for a given case area and minimal spatial unit (msu) regions")
	@DescribeResult(description = "outputs are wrapped in wfs urls for both case area and msu")
	public static String execIndicatorGreenArea(
			@DescribeParameter(name = "greenarea_wfsurl", description = "wfsurl for greenarea(polygon expected)") String greenarea_wfsurl,
			@DescribeParameter(name = "pop_msu_wfsurl", description = "wfsurl for population of minimal spatial units (polygon expected)") String msu_wfsurl,
			@DescribeParameter(name = "pop_ca_wfsurl", description = "wfsurl for population of case area (polygon explected)") String ca_wfsurl,
			@DescribeParameter(name = "pop_basenum", description = "greenarea per number of poplation (default value 100000)") int popbase,
			@DescribeParameter(name = "jobuuid", description = "job uuid, if provided, the output will be updated in that job") String jobuuid) {
		JSONObject output = new JSONObject();
		
		try {
			
			//this line enforce the geometry coordinates are in lon-lat order when result is published in GeoServer
			System.setProperty("org.geotools.referencing.forceXY", "true");
			
			//########################################
			// Check if GeoServer is reachable
			//########################################
			// this step is important if indicator outputs need to be temporarily stored on a geoserver (recommended)
			// this step can be skipped if there is no requirement of using geoserver to store outputs (NOT recommended)
			if (!Utils.isGeoServerReachable()) {
				output.put("errdesc", "GeoServer is not reachable");
				output.put("status", 1);
				return output.toString();
			}

			//########################################
			// Load transdata from UADI APIs
			//########################################
			// load datalayer from wfs url into SimpleFeatureCollection
			SimpleFeatureCollection fcGA = Utils.getFeatureCollection(greenarea_wfsurl);
			SimpleFeatureCollection fcMSU = Utils.getFeatureCollection(msu_wfsurl);
			SimpleFeatureCollection fcCA = Utils.getFeatureCollection(ca_wfsurl);
			
			
			
			//########################################
			// Core indicator calculation logic starts 
			//########################################
			// change this section to your own indicator implmentation
			if (popbase <= 0 || popbase > 99999999)
				popbase = 100000;

			// all datalayers loaded from UADI are in ESPG:4326, reprojection (whenever necessary) should be performed here 
			CoordinateReferenceSystem geoCRS = fcCA.getSchema().getCoordinateReferenceSystem();
			// determine the geo and prj CRS
			CoordinateReferenceSystem prjCRS = Utils.getAutoUTM(fcCA);

			// create a copy of projected featurecollection
			SimpleFeatureCollection fcMSUPrj = new ForceCoordinateSystemFeatureResults(fcMSU, geoCRS, false);
			fcMSUPrj = new ReprojectingFeatureCollection(fcMSUPrj, prjCRS);
			SimpleFeatureCollection fcGAPrj = new ForceCoordinateSystemFeatureResults(fcGA, geoCRS, false);
			fcGAPrj = new ReprojectingFeatureCollection(fcGAPrj, prjCRS);
			SimpleFeatureCollection fcCAPrj = new ForceCoordinateSystemFeatureResults(fcCA, geoCRS, false);
			fcCAPrj = new ReprojectingFeatureCollection(fcCAPrj, prjCRS);

			// the output geometry attribute name has to be set as "the_geom", 
			// otherwise GeoTools lib will not create a shpfile from the constructed output SimpleFeatureCollection
			String outGeomName = "the_geom";
			String gaAreaName = "gaarea";
			String indexValueName = "idxval";

			// Part 1: calculate green area index at msu level
			// build output feature type
			SimpleFeatureTypeBuilder stbMsuOutput = new SimpleFeatureTypeBuilder();
			stbMsuOutput.setName("MSUOutput");
			stbMsuOutput.setCRS(fcMSUPrj.getSchema().getCoordinateReferenceSystem());
			stbMsuOutput.add(outGeomName, MultiPolygon.class);
			stbMsuOutput.setDefaultGeometry(outGeomName);
			
			// copy attributes from msu population datalayer
			for (AttributeDescriptor attDisc : fcMSUPrj.getSchema().getAttributeDescriptors()) {
				String name = attDisc.getLocalName();
				Class type = attDisc.getType().getBinding();
				if (attDisc instanceof GeometryDescriptor) {
					// skip existing geometry field since we have created a new
					// one 'the_geom'
				} else {
					// try to keep all rest fields, since the field name cannot
					// extend 10 chars, trim it if too long.
					stbMsuOutput.add(Utils.clapFieldName(name), type);
				}
			}
			// add new attributes for holding the size (area) of green area and index value
			stbMsuOutput.add(gaAreaName, Double.class);
			stbMsuOutput.add(indexValueName, Double.class);

			// prepare output FeatureCollection
			SimpleFeatureType sftMsuOutput = stbMsuOutput.buildFeatureType();
			SimpleFeatureBuilder sfbMsuOutput = new SimpleFeatureBuilder(sftMsuOutput);
			DefaultFeatureCollection fcMSUOutputPrj = new DefaultFeatureCollection();

			SimpleFeatureIterator itrMsu = fcMSUPrj.features();

			double totalIntersectedGreenArea = 0.0;
			int totalPopulation = 0;
			while (itrMsu.hasNext()) {

				SimpleFeatureImpl f = (SimpleFeatureImpl) itrMsu.next();
				Geometry geomMSU = (Geometry) f.getDefaultGeometry();

				int population = 0;
				try {
					population = Integer.parseInt(f.getAttribute("total_population").toString());
				} catch (Exception e) {
					population = 0;
				}
				SimpleFeature newf = sfbMsuOutput.buildFeature(null);

				for (AttributeDescriptor attDisc : fcMSUPrj.getSchema().getAttributeDescriptors()) {
					String name = attDisc.getLocalName();
					// if current field is a geometry, we save it to the
					// appointed field 'the_geom'
					if (attDisc instanceof GeometryDescriptor) {
						newf.setAttribute("the_geom", f.getDefaultGeometry());
					} else // otherwise, copy field value respectively
					{
						newf.setAttribute(Utils.clapFieldName(name), f.getAttribute(name));
					}
				}

				// for each msu, find all green areas it intersects and calcuate the size of intersected area
				double intersectedGreenArea = 0.0;
				SimpleFeatureIterator itrGA = fcGAPrj.features();
				while (itrGA.hasNext()) {

					SimpleFeatureImpl fGA = (SimpleFeatureImpl) itrGA.next();
					Geometry geomGA = (Geometry) fGA.getDefaultGeometry();
					if (geomMSU.intersects(geomGA)) {
						intersectedGreenArea += geomMSU.intersection(geomGA).getArea();
					}

				}
				itrGA.close();

				// calculcate the green area index for a msu 
				double idxval = 0.0;
				if (population > 0) {
					idxval = intersectedGreenArea / (population / (popbase * 1.0));
				}
				newf.setAttribute(indexValueName, idxval);
				newf.setAttribute(gaAreaName, intersectedGreenArea);

				totalIntersectedGreenArea += intersectedGreenArea;
				totalPopulation += population;

				// add newly built feature to FeatureCollection
				fcMSUOutputPrj.add(newf);
			}
			itrMsu.close();

			// reproject msu FeatureCollection from UTM to default CRS (which is EPSG:4326)
			SimpleFeatureCollection fcMSUOutput = new ReprojectingFeatureCollection(fcMSUOutputPrj, geoCRS);

			// publish msu green area index outputs into GeoServer with nameprefix as 'ga_msu', the datalayer naming convention is "nameprefix_uuid"
			String output_msu_wfsurl = Utils.publishFeatureCollection(fcMSUOutput, "ga_msu");

			// part 2 calculate ga index at ca level, this part is similar to part 1.
			// there is no need to reporject ca since we can directly use the aggregated values of pop and greenarea of msu
			SimpleFeatureTypeBuilder stbCAOutput = new SimpleFeatureTypeBuilder();
			stbCAOutput.setName("CAOutput");
			stbCAOutput.setCRS(fcCA.getSchema().getCoordinateReferenceSystem());
			stbCAOutput.add(outGeomName, MultiPolygon.class);
			stbCAOutput.setDefaultGeometry(outGeomName);
			// copy attributes from ca population
			for (AttributeDescriptor attDisc : fcCA.getSchema().getAttributeDescriptors()) {
				String name = attDisc.getLocalName();
				Class type = attDisc.getType().getBinding();
				if (attDisc instanceof GeometryDescriptor) {
					// skip existing geometry field since we have created a new
					// one 'the_geom'
				} else {
					// try to keep all rest fields, since the field name cannot
					// extend 10 chars, trim it if too long.
					stbCAOutput.add(Utils.clapFieldName(name), type);
				}
			}
			// add new attributes for holding the size (area) of green area and index value
			stbCAOutput.add(gaAreaName, Double.class);
			stbCAOutput.add(indexValueName, Double.class);

			SimpleFeatureType sftCAOutput = stbCAOutput.buildFeatureType();
			SimpleFeatureBuilder sfbCAOutput = new SimpleFeatureBuilder(sftCAOutput);
			DefaultFeatureCollection fcCAOutput = new DefaultFeatureCollection();

			SimpleFeatureIterator itrCA = fcCA.features();

			while (itrCA.hasNext()) {

				SimpleFeatureImpl f = (SimpleFeatureImpl) itrCA.next();

				SimpleFeature newf = sfbCAOutput.buildFeature(null);

				for (AttributeDescriptor attDisc : fcCA.getSchema().getAttributeDescriptors()) {
					String name = attDisc.getLocalName();
					// if current field is a geometry, we save it to the
					// appointed field 'the_geom'
					if (attDisc instanceof GeometryDescriptor) {
						newf.setAttribute("the_geom", f.getDefaultGeometry());
					} else // otherwise, copy field value respectively
					{
						newf.setAttribute(Utils.clapFieldName(name), f.getAttribute(name));
					}
				}

				double idxval = 0.0;
				if (totalPopulation > 0) {
					idxval = totalIntersectedGreenArea / (totalPopulation / (popbase * 1.0));
				}
				newf.setAttribute(indexValueName, idxval);
				newf.setAttribute(gaAreaName, totalIntersectedGreenArea);

				// add newly built feature to FeatureCollection
				fcCAOutput.add(newf);
			}
			itrCA.close();

			// publish msu results into geoserver
			String output_ca_wfsurl = Utils.publishFeatureCollection(fcCAOutput, "ga_ca");
			
			//########################################
			// Core indicator calculation logic ends
			//########################################
			
			//########################################
			// Wrap up indicator outputs
			//########################################
			// prepare for the output json MUST be organised in following structure:
			// { "status": 0,
			//	 "data" :{
			//		"geolayers":[
			//						{
			//							"layername":"WORKSPACE_NAME:DATALAYER_NAME",
			//							"layerdisplayname":"A_READABLE_NAME_FOR_YOUR_INDICATOR_OUTPUT_DATALAYER",
			//							"bbox":[],
			//							"wfs": {
			//								"url":"",
			//								//ATTENTION: wfs style is NOT used in UADI currently, it is reserved for future development
			//								"styleparams":{ // if provided, this can be used for automatic client-side style creation
			//										"attrname":"", //attribute name that the style is created for
			//										"minval":min,
			//										"maxval":max,
			//										"meanval":mean,
			//										"sdval": standard deviation
			//										}
			//									},
			//							"wms": {
			//								"url":"http://xxxx/geoserver/WORKSPACE_NAME/wms",
			//								//ATTENTION: wms style is mandatory and MUST be created so that the layer can be visualised in UADI.
			//								"styleparams":{
			//										"attrname":"", //attribute name that the style is created for
			//										"stylename":"",
			//										"sldurl":"",
			//										"legendurl":""
			//										}
			//									}
			//						}
			//					],
			//  	"textlines":[//this structure is mainly designed for storing R console outputs (e.g. regression model)
            //     			 "line1","line2","line3","lineN"
            //			],
			//		"tables":[//this structure is complex enough to describe a matrix data like csv
			//					{
			//						"title": "Your Table Title",
			//						"data":[
			//					              {
			//					            	  "colname":"column1",
			//					            	  "values":[0,1,2,3,4,5]
			//					              },
			//					              {
			//					            	  "colname":"column2",
			//				            	  	  "values":["Azadeh","Sam","Benny","Soheil","Mohsen","Abbas"]
			//					              }
			//								]
			//					}		
			//		         ],
			//		"charts":[ // this contains data structure for 8 types of charts supported by UADI including:
			//				   // columnchart, barchart, linechart, areachart, radialchart, piechart, donutchart, scatterchart 
			//		              {
			//						  "type" : "columnchart", // "barchart", "linechart", "areachart" and "radialchart" shares the exactly same data structure of "columnchart"
			//		            	  "seqnum":1, // the visualisation sequence of charts 
			//						  "width":600, // width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
			//						  "height":400, // height of the chart, if not provided, default value on the visualisation page will be applied. 
			//						  "xaxistitle":"", //title for x axis
			//						  "yaxistitle":"", //title for y axis
			//						  "xaxislabelsuffix":"", //a suffix for labels on x axis, e.g., %, min, sec
			//						  "yaxislabelsuffix":"", //a suffix for labels on y axis, e.g., %, min, sec
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "stacked": false, // whether to put yfileds on stack, this ONLY applies to "columnchart" and "barchart" when multiple yfields provided
			//						  "xfield": "month", // a categorical attribute 
			//						  "yfield": ["data1", "data2", "data3", "data4"], // an array of numeric y field(s), even with one element
			//						  "yfieldtitle": [ "IE", "Firefox", "Chrome", "Safari"], // an array of title of y field(s) used in legend, even with one element  
			//						  "data": [// prepared the data in json array format, xfield should be in the first column, the rest should be yfields 
			//					                { "month": "Jan", "data1": 20, "data2": 37, "data3": 35, "data4": 4 },
			//					                { "month": "Feb", "data1": 20, "data2": 37, "data3": 36, "data4": 5 },
			//					                { "month": "Mar", "data1": 19, "data2": 36, "data3": 37, "data4": 4 },
			//					                { "month": "Apr", "data1": 18, "data2": 36, "data3": 38, "data4": 5 },
			//					                { "month": "May", "data1": 18, "data2": 35, "data3": 39, "data4": 4 },
			//					                { "month": "Jun", "data1": 17, "data2": 34, "data3": 42, "data4": 4 },
			//					                { "month": "Jul", "data1": 16, "data2": 34, "data3": 43, "data4": 4 },
			//					                { "month": "Aug", "data1": 16, "data2": 33, "data3": 44, "data4": 4 },
			//					                { "month": "Sep", "data1": 16, "data2": 32, "data3": 44, "data4": 4 },
			//					                { "month": "Oct", "data1": 16, "data2": 32, "data3": 45, "data4": 4 },
			//					                { "month": "Nov", "data1": 15, "data2": 31, "data3": 46, "data4": 4 },
			//					                { "month": "Dec", "data1": 15, "data2": 31, "data3": 47, "data4": 4 }
			//					            ]
			//		              },
			//		              {
			//						  "type" : "piechart",  //"donutchart" shares the exactly same data structure of "piechart"
			//		            	  "seqnum":2, // the visualisation sequence of charts 
			//						  "width":600, // width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
			//						  "height":400, // height of the chart, if not provided, default value on the visualisation page will be applied.
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "donut": false, // set false to create a piechart, set true to create a donutchart
			//						  "xfield": "month", // a categorical attribute 
			//						  "yfield": "data1", // a numeric attribute 
			//						  "data": [// prepared the data in json array format, xfield should be in the first column, the rest should be yfield 
			//									{ "month": "Jan", "data1": 68 },
            //									{ "month": "Feb", "data1": 17 },
            //									{ "month": "Mar", "data1": 15 }
			//					            ]
			//		              },
			//		              {
			//						  "type" : "scatterchart",
			//		            	  "seqnum":3, // the visualisation sequence of charts 
			//						  "width":600, // width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
			//						  "height":400, // height of the chart, if not provided, default value on the visualisation page will be applied. 
			//						  "xaxistitle":"", //title for x axis
			//						  "yaxistitle":"", //title for y axis
			//						  "xaxislabelsuffix":"", //a suffix for labels on x axis, e.g., %, min, sec
			//						  "yaxislabelsuffix":"", //a suffix for labels on y axis, e.g., %, min, sec
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "xfield": "x", // a numeric attribute 
			//						  "yfield": "y", // a numeric attribute 
			//						  "data": [// prepared the data in json array format 
			//									{ "x": 5, "y": 20 },
            //									{ "x": 480, "y": 90 },
            //									{ "x": 250, "y": 50 },
            //									{ "x": 100, "y": 33 },
            //									{ "x": 330, "y": 95 },
            //									{ "x": 410, "y": 12 },
            //									{ "x": 475, "y": 44 }
			//					            ]
			//		              }
			//		            ]
			//
			//
			//	},
			//   "errdesc": ""
			// }
			// "status" 0 means no error occurrs during the processing, 1 means processing has errors. Integer 0 or 1 (rather than string "0" or "1") is expected. 
			// "data" attribte can be either JSONObject (recommended) or JSONArray, it is the place where the outputs should be stored. 
			// "errdesc" contains the error descriptions, it should be empty when "status" sets to 0			
			JSONObject data = new JSONObject();
			
			JSONArray geolayerArr = new JSONArray();
			
			//output part 1/2: casearea output layer
			JSONObject geolayer_ca = new JSONObject();
			geolayer_ca.put("layerdisplayname", "greenarea_ca");
			geolayer_ca.put("layername", Utils.getLayerNameFromWFSUrl(output_ca_wfsurl));
			geolayer_ca.put("bbox", Utils.getBbox(fcCA));
			
			//wfs part
			JSONObject wfs_ca = new JSONObject();
			wfs_ca.put("url", output_ca_wfsurl);
			/*
			//if necessary, get wfs style parameters created
			JSONObject caWFSStyleResult = Utils.createWFSStyle(output_ca_wfsurl, "idxval");
			if(caWFSStyleResult!=null && caWFSStyleResult.getInt("status") == 0){
				wfs_ca.put("styleparams", caWFSStyleResult.getJSONObject("data"));
			}
			*/
			//or, just skip it (this option is suggested)
			wfs_ca.put("styleparams", new JSONObject());
			
			geolayer_ca.put("wfs", wfs_ca);
			
			//wms part, get wms style parameters created
			//ATTENTION: wms style MUST be created so that the layer can be visualised in UADI.
			JSONObject caWMSStyleResult = Utils.createWMSStyle(output_ca_wfsurl, "idxval", "Greens", false, "polygon", 1, "Jenks");
			if(caWMSStyleResult!=null && caWMSStyleResult.getInt("status") == 0){
				JSONObject wms_ca = new JSONObject();
				wms_ca.put("url", Utils.getWMSUrlTemplate());
				wms_ca.put("styleparams", caWMSStyleResult.getJSONObject("data"));
				geolayer_ca.put("wms", wms_ca);
			}

			geolayerArr.put(geolayer_ca);
			
			//output part 2/2: msu output layer
			JSONObject geolayer_msu = new JSONObject();
			geolayer_msu.put("layerdisplayname", "greenarea_msu");
			geolayer_msu.put("layername", Utils.getLayerNameFromWFSUrl(output_msu_wfsurl));
			geolayer_msu.put("bbox", Utils.getBbox(fcMSU));
			
			//wfs part
			JSONObject wfs_msu = new JSONObject();
			wfs_msu.put("url", output_msu_wfsurl);
			/*
			//if necessary, get wfs style parameters created
			JSONObject msuWFSStyleResult = Utils.createWFSStyle(output_msu_wfsurl, "idxval");
			if(msuWFSStyleResult!=null && msuWFSStyleResult.getInt("status") == 0){
				wfs_msu.put("styleparams", msuWFSStyleResult.getJSONObject("data"));
			}
			*/
			//or, just skip it (this option is suggested)
			wfs_msu.put("styleparams", new JSONObject());
			
			geolayer_msu.put("wfs", wfs_msu);
			
			//wms part, get wms style parameters created
			//ATTENTION: wms style MUST be created so that the layer can be visualised in UADI.
			JSONObject msuWMSStyleResult = Utils.createWMSStyle(output_msu_wfsurl, "idxval", "Greens", false, "polygon", 9, "Jenks");
			if(msuWMSStyleResult!=null && msuWMSStyleResult.getInt("status") == 0){
				JSONObject wms_msu = new JSONObject();
				wms_msu.put("url", Utils.getWMSUrlTemplate());
				wms_msu.put("styleparams", msuWMSStyleResult.getJSONObject("data"));
				geolayer_msu.put("wms", wms_msu);
			}
			
			geolayerArr.put(geolayer_msu);
			
			data.put("geolayers", geolayerArr);
			output.put("data", data);
			output.put("status", 0);

		} catch (Exception e) {
			output.put("errdesc", e.getMessage());
			output.put("status", 1);
		}

		//########################################
		// Sync indicator outputs to an indicator job
		//########################################
		// update job outputs if jobuuid is provided
		if (jobuuid != null) {
			System.out.println("=== output:"+output.toString());
			Utils.updateJob(output, jobuuid);
		}

		return output.toString();
	}

}